﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using HBsis.Livraria.Domain.Entidades;
using Hbsis.Livraria.Dominio;

namespace HBsis.Livraria.API.Controllers
{
    public class LivrosController : ApiController
    {
        private LivrariaContext db = new LivrariaContext();

        // GET: api/Livros
        public IQueryable<Livros> GetLivros()
        {
            return db.Livros;
        }

        // GET: api/Livros/5
        [ResponseType(typeof(Livros))]
        public IHttpActionResult GetLivros(int id)
        {
            Livros livros = db.Livros.Find(id);
            if (livros == null)
            {
                return NotFound();
            }

            return Ok(livros);
        }

        // PUT: api/Livros/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLivros(int id, Livros livros)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != livros.ID)
            {
                return BadRequest();
            }

            db.Entry(livros).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LivrosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Livros
        [ResponseType(typeof(Livros))]
        public IHttpActionResult PostLivros(Livros livros)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Livros.Add(livros);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = livros.ID }, livros);
        }

        // DELETE: api/Livros/5
        [ResponseType(typeof(Livros))]
        public IHttpActionResult DeleteLivros(int id)
        {
            Livros livros = db.Livros.Find(id);
            if (livros == null)
            {
                return NotFound();
            }

            db.Livros.Remove(livros);
            db.SaveChanges();

            return Ok(livros);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LivrosExists(int id)
        {
            return db.Livros.Count(e => e.ID == id) > 0;
        }
    }
}