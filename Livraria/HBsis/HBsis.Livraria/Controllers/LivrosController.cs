﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HBsis.Livraria.Domain.Entidades;
using Hbsis.Livraria.Dominio;

namespace HBsis.Livraria.Controllers
{
    public class LivrosController : Controller
    {
        private LivrariaContext db = new LivrariaContext();

        // GET: Livros
        public ActionResult Index()
        {
            var livros = db.Livros.Include(l => l.Autor).OrderBy(l => l.Nome);
            return View(livros.ToList());
        }

        // GET: Livros/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Livros livros = db.Livros.Include(l => l.Autor).Where(l => l.ID == id).FirstOrDefault();
            if (livros == null)
            {
                return HttpNotFound();
            }
            return View(livros);
        }

        // GET: Livros/Create
        public ActionResult Create()
        {
            ViewBag.Autor_ID = new SelectList(db.Autor, "ID", "Nome");
            return View();
        }

        // POST: Livros/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Nome,Autor_ID,Descricao,DataPublicacao")] Livros livros)
        {
            if (ModelState.IsValid)
            {
                db.Livros.Add(livros);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Autor_ID = new SelectList(db.Autor, "ID", "Nome", livros.Autor_ID);
            return View(livros);
        }

        // GET: Livros/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Livros livros = db.Livros.Include(l => l.Autor).Where(l => l.ID == id).FirstOrDefault();
            if (livros == null)
            {
                return HttpNotFound();
            }
            ViewBag.Autor_ID = new SelectList(db.Autor, "ID", "Nome", livros.Autor_ID);
            return View(livros);
        }

        // POST: Livros/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Nome,Autor_ID,Descricao,DataPublicacao")] Livros livros)
        {
            if (ModelState.IsValid)
            {
                db.Entry(livros).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Autor_ID = new SelectList(db.Autor, "ID", "Nome", livros.Autor_ID);
            return View(livros);
        }

        // GET: Livros/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Livros livros = db.Livros.Include(l => l.Autor).Where(l => l.ID == id).FirstOrDefault();
            if (livros == null)
            {
                return HttpNotFound();
            }
            return View(livros);
        }

        // POST: Livros/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Livros livros = db.Livros.Find(id);
            db.Livros.Remove(livros);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
