﻿using System;
using System.Collections.Generic;
using C6Consulting.RockPaperScissor.CustomExceptions;
using Newtonsoft.Json;

namespace C6Consulting.RockPaperScissor
{

    class Program
    {
        static List<dynamic> ListaAtual { get; set; }
        static String TorneioAtual { get; set; }

        static void Main(string[] args)
        {
            // 2 players game
            Player winner = RpsGameWinner("[ ['Armando', 'P'], ['Dave', 'S'] ] ");
            Console.WriteLine("Two players match:");
            Console.WriteLine(String.Format("Winner: {0}, Move: {1}", winner.Name, winner.Move));
            Console.ReadLine();

            // n players game
            Player winnerTournament = RpsTournamentWinner("[" +
                                                                "[" +
                                                                    "[" +
                                                                        "['Armando', 'P'], " +
                                                                        "['Dave', 'S']" +
                                                                    "]," +
                                                                    "[ " +
                                                                        "['Richard', 'R'], " +
                                                                        "['Michael', 'S']" +
                                                                    "]" +
                                                                "], " +
                                                                "[" +
                                                                    "[" +
                                                                        "['Allen', 'S']," +
                                                                        "['Omer', 'P']" +
                                                                    "]," +
                                                                    "[" +
                                                                        "['David E.', 'R']," +
                                                                        "['Richard X.', 'P']" +
                                                                    "]" +
                                                               "]" +
                                                            "] "
            );
            Console.WriteLine("Tournament:");
            Console.WriteLine(String.Format("Winner: {0}, Move: {1}", winnerTournament.Name, winnerTournament.Move));
            //Console.WriteLine(String.Format("{0}", winnerTournament));
            Console.ReadLine();
        }

        static Player RpsTournamentWinner(String encodedTournament, List<Player> output = null) //, String globalResult = null)
        {
            Player winner = new Player();

            bool rootExec = false; // Controla se a chamada da função é a primeira das chamadas encadeadas
            if (output == null)
            {
                rootExec = true;
                output = new List<Player>();
            }
            dynamic objInput = JsonConvert.DeserializeObject(encodedTournament);

            // 1st execution
            /*if (globalResult == null)
            {
                globalResult = encodedTournament;
            }*/

            dynamic resultTree = objInput;
            Type type;
            int i = 0;

            foreach (var aux in objInput)
            {
                type = aux[0].GetType();
                if (type.Name == "JValue")
                {

                    /*String p1Name = objInput[0][0].ToString();
                    String p1Move = objInput[0][1].ToString();

                    String p2Name = objInput[1][0].ToString();
                    String p2Move = objInput[1][1].ToString();*/

                    winner = RpsGameWinner(JsonConvert.SerializeObject(objInput));

                    objInput[0] = winner.Name;
                    objInput[1] = winner.Move;

                    //dynamic tmpResult = JsonConvert.DeserializeObject(globalResult);

                    return winner;
                    //break;

                    //tmpResult[level] = objInput;

                    //ListaAtual[actualLevel].Add(new Player(p1Name, p1Move));
                    //ListaAtual[actualLevel].Add(new Player(p2Name, p2Move));
                }
                else
                {
                    //ListaAtual.Add(null);
                    //path += String.Format("{0}", i.ToString());

                    winner = RpsTournamentWinner(JsonConvert.SerializeObject(aux), output); //, globalResult);
                    if (winner != null)
                        output.Add(winner);

                    //path = path.Remove(path.Length - 1);
                    //actualLevel = --level;
                }
                i++;
            }

            if (rootExec)
            {
                /*if (output.ToArray().Length == 1)
                {
                    return output[0];
                }
                else */
                if (output.ToArray().Length == 2)
                {
                    return RpsGameWinner(String.Format(" [ ['{0}','{1}'], ['{2}','{3}'] ] ", output[0].Name, output[0].Move, output[1].Name, output[1].Move));
                }
                else
                {
                    String singleLevel = " [ ";
                    for (i = 0; i < output.ToArray().Length; i += 2)
                    {
                        var xpto = new List<Player>();
                        singleLevel += " [ ";
                        singleLevel += String.Format(" [ '{0}', '{1}' ], ", output[i].Name, output[i].Move);
                        singleLevel += String.Format(" [ '{0}', '{1}' ] ", output[i + 1].Name, output[i + 1].Move);
                        singleLevel += " ], ";
                    }
                    singleLevel += " ] ";

                    winner = RpsTournamentWinner(singleLevel);
                    return winner;
                }
            }

            return null;
        }

        static Player RpsGameWinner(String encodedPlayer)
        {
            dynamic objInput = JsonConvert.DeserializeObject(encodedPlayer);

            List<Player> players = new List<Player>();
            int playersNumber = 0;
            foreach (var aux in objInput)
            {
                playersNumber++;
                String auxMove = aux[1].ToString().ToUpper();
                if (auxMove != "R" && auxMove != "P" && auxMove != "S")
                {
                    throw new NoSuchStrategyError("Wrong move. Only use R, P or S (Rock, Paper or Scissor)!");
                }

                players.Add(new Player(aux[0].ToString(), auxMove));
            }

            if (playersNumber != 2)
            {
                throw new WrongNumberOfPlayersError("Wrong number of players!");
            }

            Player winner = new Player();
            if (players[0].Move == "R")
            {
                if (players[1].Move == "P")
                    winner = players[1];
                else
                    winner = players[0];
            }
            if (players[0].Move == "P")
            {
                if (players[1].Move == "S")
                    winner = players[1];
                else
                    winner = players[0];
            }
            if (players[0].Move == "S")
            {
                if (players[1].Move == "R")
                    winner = players[1];
                else
                    winner = players[0];
            }

            return winner;
        }
    }
}
