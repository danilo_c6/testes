﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C6Consulting.RockPaperScissor
{
    class Player
    {
        public String Name { get; set; }
        public String Move { get; set; }

        public Player() { }

        public Player(String ParamName, String ParamMove)
        {
            Name = ParamName;
            Move = ParamMove;
        }
    }
}
