﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C6Consulting.RockPaperScissor.CustomExceptions
{
    class NoSuchStrategyError : Exception
    {
        public NoSuchStrategyError(string message) : base(message)
        {
        }
    }
}
